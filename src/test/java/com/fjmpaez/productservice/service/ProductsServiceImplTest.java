package com.fjmpaez.productservice.service;

import static com.fjmpaez.productservice.util.FileUtils.readFileFromClasspath;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productservice.client.AdidasClient;
import com.fjmpaez.productservice.client.ProductReviewsFeignClient;
import com.fjmpaez.productservice.exception.ErrorEnum;
import com.fjmpaez.productservice.pojo.Review;

import mjson.Json;

@RunWith(MockitoJUnitRunner.class)
public class ProductsServiceImplTest {

    @InjectMocks
    private ProductsServiceImpl productsService;

    @Mock
    private Utils utils;

    @Mock
    private AdidasClient adidasClient;

    @Mock
    private ProductReviewsFeignClient productReviewsFeignClient;

    @Before
    public void setUp() throws Exception {
        when(utils.getCurrentCorrelationId()).thenReturn("correlation1234test");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductWithValidProductId() {

        Json json = Json.read(readFileFromClasspath("mock/adidas_C77154.json"));
        when(adidasClient.getProduct("C77154")).thenReturn(json);

        Review Review = new Review(10, 2);
        when(productReviewsFeignClient.getReview(null, "C77154")).thenReturn(Review);

        Object result = productsService.getProduct("C77154");

        assertThat(result, notNullValue());

        Map<String, Object> body = (HashMap<String, Object>) result;
        Integer reviewsAvgScore = (Integer) body.get("reviews_avg_score");
        Integer reviewsCount = (Integer) body.get("reviews_count");
        assertThat(reviewsAvgScore, is(10));
        assertThat(reviewsCount, is(2));

        verify(adidasClient).getProduct("C77154");
        verify(productReviewsFeignClient).getReview(null, "C77154");
        verifyNoMoreInteractions(adidasClient, productReviewsFeignClient);
    }

    @Test
    public void getProductWithoutProductId() {

        try {
            productsService.getProduct(null);
            fail();
        } catch (Exception e) {
            assertThat(e.getMessage(), is("productId must not be null"));
        }

        verifyZeroInteractions(adidasClient, productReviewsFeignClient);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductWithValidProductIdNoReview() {

        Json json = Json.read(readFileFromClasspath("mock/adidas_C77154.json"));
        when(adidasClient.getProduct("C77154")).thenReturn(json);

        when(productReviewsFeignClient.getReview(null, "C77154")).thenReturn(null);

        Object result = productsService.getProduct("C77154");

        assertThat(result, notNullValue());

        Map<String, Object> body = (HashMap<String, Object>) result;
        Integer reviewsAvgScore = (Integer) body.get("reviews_avg_score");
        Integer reviewsCount = (Integer) body.get("reviews_count");
        assertThat(reviewsAvgScore, nullValue());
        assertThat(reviewsCount, nullValue());

        verify(adidasClient).getProduct("C77154");
        verify(productReviewsFeignClient).getReview(null, "C77154");
        verifyNoMoreInteractions(adidasClient, productReviewsFeignClient);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductWithValidProductIdReviewServiceNotAvailable() {

        Json json = Json.read(readFileFromClasspath("mock/adidas_C77154.json"));
        when(adidasClient.getProduct("C77154")).thenReturn(json);

        when(productReviewsFeignClient.getReview(null, "C77154")).thenThrow(new RuntimeException());

        Object result = productsService.getProduct("C77154");

        assertThat(result, notNullValue());

        Map<String, Object> body = (HashMap<String, Object>) result;
        Integer reviewsAvgScore = (Integer) body.get("reviews_avg_score");
        Integer reviewsCount = (Integer) body.get("reviews_count");
        assertThat(reviewsAvgScore, nullValue());
        assertThat(reviewsCount, nullValue());

        verify(adidasClient).getProduct("C77154");
        verify(productReviewsFeignClient).getReview(null, "C77154");
        verifyNoMoreInteractions(adidasClient, productReviewsFeignClient);
    }

    @Test
    public void getProductWithValidProductIdNonExist() {

        when(adidasClient.getProduct("C77154"))
                .thenThrow(new DefinedErrorException(ErrorEnum.NOT_FOUND));

        try {
            productsService.getProduct("C77154");
            fail();
        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.NOT_FOUND));
        } catch (Exception e) {
            fail();
        }

        verify(adidasClient).getProduct("C77154");
        verifyNoMoreInteractions(adidasClient, productReviewsFeignClient);
    }

}
