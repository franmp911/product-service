package com.fjmpaez.productservice.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {

    private FileUtils() {
    }

    public static String readFileFromClasspath(String resourceName) {
        try {
            java.net.URL systemResource = ClassLoader.getSystemResource(resourceName);
            if (systemResource == null) {
                throw new RuntimeException("Resource not found: " + resourceName);
            }
            return new String(Files.readAllBytes(Paths.get(systemResource.toURI())));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
