package com.fjmpaez.productservice.controller;

import static com.fjmpaez.productservice.util.FileUtils.readFileFromClasspath;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productservice.ProductServiceApplication;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ProductServiceApplication.class)
public class ProductsControllerTest {

    private static final String GET_PRODUCT_URL = "http://localhost:8667/v1.0/product/{product_id}";

    private MockMvc mockMvc;

    @ClassRule
    public static WireMockClassRule adidasWireMockStatic = new WireMockClassRule(80);

    @ClassRule
    public static WireMockClassRule reviewsWireMockStatic = new WireMockClassRule(8666);

    @Rule
    public WireMockClassRule configWireMock = adidasWireMockStatic;

    @Autowired
    private ProductsController productsController;

    @MockBean
    private Utils utils;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(productsController).build();
        when(utils.getCurrentCorrelationId()).thenReturn("correlation1234test");
    }

    @Test
    public void getProductWithValidProductIdAndReview() throws Exception {

        adidasWireMock(200, "mock/adidas_C77154.json");
        reviewsWireMock(200, "mock/review_C77154.json");

        mockMvc.perform(get(GET_PRODUCT_URL, "C77154")).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.reviews_avg_score", Matchers.is(10)))
                .andExpect(jsonPath("$.reviews_count", Matchers.is(2)))
                .andExpect(jsonPath("$.*", Matchers.hasSize(15)));
    }

    @Test
    public void getProductWithValidProductIdNoReview() throws Exception {

        adidasWireMock(200, "mock/adidas_C77154.json");
        reviewsWireMock(404, "mock/review_C77154_not_found.json");

        mockMvc.perform(get(GET_PRODUCT_URL, "C77154")).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is("C77154")))
                .andExpect(jsonPath("$.*", Matchers.hasSize(13)));
    }

    @Test(expected = Exception.class)
    public void getProductWithoutValidProductId() throws Exception {

        adidasWireMock(404, "mock/adidas_product_not_found.json");

        mockMvc.perform(get(GET_PRODUCT_URL, "C77154")).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.error_code", Matchers.is("002003")))
                .andExpect(jsonPath("$.description", Matchers.is("product not found")))
                .andExpect(jsonPath("$.correlation_id", Matchers.is("correlation1234test")));
    }

    private static void adidasWireMock(int expectedStatus, String fileExpectedBody) {
        adidasWireMockStatic.stubFor(get(urlPathMatching("/api/products/.*")).willReturn(aResponse()
                .withStatus(expectedStatus).withHeader("Content-Type", MediaType.APPLICATION_JSON)
                .withBody(readFileFromClasspath(fileExpectedBody))));
    }

    private static void reviewsWireMock(int expectedStatus, String fileExpectedBody) {
        reviewsWireMockStatic.stubFor(get(urlPathMatching("/v1\\.0/review/.*"))
                .willReturn(aResponse().withStatus(expectedStatus)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON)
                        .withBody(readFileFromClasspath(fileExpectedBody))));
    }

}
