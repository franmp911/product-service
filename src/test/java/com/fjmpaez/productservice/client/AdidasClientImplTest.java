package com.fjmpaez.productservice.client;

import static com.fjmpaez.productservice.util.FileUtils.readFileFromClasspath;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.productservice.exception.ErrorEnum;
import com.google.common.collect.Lists;

import mjson.Json;

@RunWith(MockitoJUnitRunner.class)
public class AdidasClientImplTest {

    private static final String ADIDAS_MOCK_URL = "http://adidas.mock/api/products/{product_id}";
    static final String ID = "id";
    private static final String PRODUCT_ID = "product_id";

    @InjectMocks
    private AdidasClientImpl adidasClientImpl;

    @Mock
    private RestTemplate template;

    @Captor
    private ArgumentCaptor<HttpEntity<?>> httpEntityCaptor;

    @Before
    public void setUp() throws Exception {

        ReflectionTestUtils.setField(adidasClientImpl, "adidasUrl", ADIDAS_MOCK_URL);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductWithValidProductId() {

        when(template.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class),
                any(Class.class), any(Map.class)))
                        .thenReturn(buildClientResponse("mock/adidas_C77154.json", HttpStatus.OK));

        String productId = "C77154";

        HashMap<String, String> params = new HashMap<>(1);
        params.put(PRODUCT_ID, productId);

        Json json = adidasClientImpl.getProduct(productId);

        verify(template).exchange(eq(ADIDAS_MOCK_URL), eq(HttpMethod.GET),
                httpEntityCaptor.capture(), eq(String.class), eq(params));

        assertThat(httpEntityCaptor.getValue()).hasFieldOrProperty("headers").isNotNull();
        assertThat(httpEntityCaptor.getValue().getHeaders())
                .containsOnly(entry("user-agent", Lists.newArrayList("xxx")));

        assertThat(json.isNull(), is(false));
        assertThat(json.isObject(), is(true));
        assertThat(json.has(ID), is(true));

        verifyNoMoreInteractions(template);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductProductNotFound() {

        when(template.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class),
                any(Class.class), any(Map.class))).thenReturn(
                        buildClientResponse("mock/adidas_product_not_found.json", HttpStatus.OK));

        String productId = "C77154mmmm";

        HashMap<String, String> params = new HashMap<>(1);
        params.put(PRODUCT_ID, productId);

        try {
            adidasClientImpl.getProduct(productId);
            fail();
        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.NOT_FOUND));
        } catch (Exception e) {
            fail();
        }

        verify(template).exchange(eq(ADIDAS_MOCK_URL), eq(HttpMethod.GET),
                httpEntityCaptor.capture(), eq(String.class), eq(params));

        assertThat(httpEntityCaptor.getValue()).hasFieldOrProperty("headers").isNotNull();
        assertThat(httpEntityCaptor.getValue().getHeaders())
                .containsOnly(entry("user-agent", Lists.newArrayList("xxx")));

        verifyNoMoreInteractions(template);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getProductResponseEntity500() {

        when(template.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class),
                any(Class.class), any(Map.class)))
                        .thenReturn(buildClientResponse(null, HttpStatus.INTERNAL_SERVER_ERROR));

        String productId = "C77154mmmm";

        HashMap<String, String> params = new HashMap<>(1);
        params.put(PRODUCT_ID, productId);

        try {
            adidasClientImpl.getProduct(productId);
            fail();
        } catch (DefinedErrorException e) {
            assertThat(e.getErrorEnum(), is(ErrorEnum.EXTERNAL_ERROR));
        } catch (Exception e) {
            fail();
        }

        verify(template).exchange(eq(ADIDAS_MOCK_URL), eq(HttpMethod.GET),
                httpEntityCaptor.capture(), eq(String.class), eq(params));

        assertThat(httpEntityCaptor.getValue()).hasFieldOrProperty("headers").isNotNull();
        assertThat(httpEntityCaptor.getValue().getHeaders())
                .containsOnly(entry("user-agent", Lists.newArrayList("xxx")));

        verifyNoMoreInteractions(template);
    }

    @Test
    public void getProductWithoutProductId() {

        try {
            adidasClientImpl.getProduct(null);
            fail();
        } catch (Exception e) {
            assertThat(e.getMessage(), is("productId must not be null"));
        }

        verifyZeroInteractions(template);
    }

    private ResponseEntity<String> buildClientResponse(String file, HttpStatus httpStatus) {

        String responseBody = null;

        if (Objects.nonNull(file)) {
            responseBody = readFileFromClasspath(file);
        }

        return new ResponseEntity<String>(responseBody, httpStatus);
    }

}
