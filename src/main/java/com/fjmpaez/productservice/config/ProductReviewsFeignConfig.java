package com.fjmpaez.productservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;
import feign.Request;

@Configuration
public class ProductReviewsFeignConfig {

    @Value("${feign.client.config.default.connect-timeout:60000}")
    private int connectTimeout;

    @Value("${feign.client.config.default.read-timeout:60000}")
    private int readTimeout;

    @Bean
    public Request.Options options() {
        return new Request.Options(connectTimeout, readTimeout);
    }

    @Bean
    public Logger.Level feignLogger() {
        return Logger.Level.FULL;
    }
}
