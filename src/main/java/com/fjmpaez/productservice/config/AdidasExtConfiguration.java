package com.fjmpaez.productservice.config;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "adidas.service")
public class AdidasExtConfiguration {

    @NotBlank
    private String protocol;

    @NotBlank
    private String host;

    @Value("${timeout:60000}")
    private int timeout;

    @NotBlank
    private String path;

}
