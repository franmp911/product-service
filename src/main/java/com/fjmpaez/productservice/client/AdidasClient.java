package com.fjmpaez.productservice.client;

import mjson.Json;

public interface AdidasClient {

    static final String ID = "id";
    static final String PRODUCT_ID = "product_id";

    Json getProduct(String productId);

}
