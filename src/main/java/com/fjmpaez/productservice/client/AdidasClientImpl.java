package com.fjmpaez.productservice.client;

import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.productservice.exception.ErrorEnum;

import lombok.extern.slf4j.Slf4j;
import mjson.Json;

@Slf4j
@Service
public class AdidasClientImpl implements AdidasClient {

    private static final String ERROR_MESSAGE = "message";

    @Autowired
    @Qualifier("adidasRestTemplate")
    private RestTemplate template;

    @Autowired
    @Qualifier("adidasUrl")
    private String adidasUrl;

    @Override
    public Json getProduct(String productId) {

        Objects.requireNonNull(productId, "productId must not be null");

        log.trace("getProduct {}", productId);

        Json json = null;

        UriComponentsBuilder builder;
        builder = UriComponentsBuilder.fromHttpUrl(adidasUrl);

        HashMap<String, String> params = buildPath(productId);

        String externalErrorCode = null;

        try {

            final String finalUrl = URLDecoder.decode(builder.build().encode().toUriString(),
                    StandardCharsets.UTF_8.name());

            log.debug("invoking external URL {} - {}", finalUrl, params);

            ResponseEntity<String> externalResponse = template.exchange(finalUrl, HttpMethod.GET,
                    getHttpEntity(), String.class, params);

            json = parseExternalResponse(externalResponse);

        } catch (RestClientException e) {

            if (e instanceof RestClientResponseException) {

                RestClientResponseException restEx = (RestClientResponseException) e;
                externalErrorCode = getReason(restEx);
                log.error("External error: {}", externalErrorCode, e);

                if (HttpStatus.NOT_FOUND.value() == restEx.getRawStatusCode()) {
                    throw new DefinedErrorException(ErrorEnum.NOT_FOUND, e);
                }
            }

            checkTimeoutException(e, adidasUrl);

            throw new DefinedErrorException(ErrorEnum.EXTERNAL_ERROR, null, externalErrorCode, e);

        } catch (DefinedErrorException e) {
            throw e;
        } catch (Exception e) {
            log.error("error requesting: {}", adidasUrl, e);
            throw new DefinedErrorException(ErrorEnum.INTERNAL_SERVER_ERROR, e);
        }

        log.trace("getProduct - response {}", json);

        return json;
    }

    private Json parseExternalResponse(ResponseEntity<String> externalResponse) {

        Objects.requireNonNull(externalResponse, "response from adidas must not be null");

        Json json = null;

        if (HttpStatus.OK.equals(externalResponse.getStatusCode())) {

            String response = externalResponse.getBody();

            Objects.requireNonNull(response, "response body from adidas must not be null");

            json = Json.read(response);

            if (json.isNull() || !json.isObject() || !json.has(ID)) {
                throw new DefinedErrorException(ErrorEnum.NOT_FOUND);
            }
        } else {
            throw new DefinedErrorException(ErrorEnum.EXTERNAL_ERROR, null, "wrong HttpStatus");
        }

        return json;
    }

    private HashMap<String, String> buildPath(String productId) {

        HashMap<String, String> params = new HashMap<>(1);

        if (StringUtils.isNotBlank(productId)) {
            params.put(PRODUCT_ID, productId);
        } else {
            throw new DefinedErrorException(ErrorEnum.NOT_FOUND);
        }

        return params;
    }

    private HttpEntity<?> getHttpEntity() {

        // User-Agent header can not contains "HttpClient"
        HttpHeaders headers = new HttpHeaders();
        headers.set("user-agent", "xxx");

        return new HttpEntity<>(null, headers);
    }

    private void checkTimeoutException(Exception e, String url) {

        if (e instanceof RestClientResponseException) {

            RestClientResponseException restE = (RestClientResponseException) e;

            if (HttpStatus.GATEWAY_TIMEOUT.value() == restE.getRawStatusCode()) {
                throw new DefinedErrorException(ErrorEnum.TIMEOUT, url, e);
            }

        } else if (e instanceof SocketTimeoutException) {

            throw new DefinedErrorException(ErrorEnum.TIMEOUT, url, e);

        } else {

            Throwable root = getRootCause(e);

            if (root instanceof SocketTimeoutException) {
                throw new DefinedErrorException(ErrorEnum.TIMEOUT, url, root);
            }
        }
    }

    private Throwable getRootCause(Exception e) {

        Throwable cause = e;

        while (cause.getCause() != null) {
            cause = cause.getCause();
        }

        return cause;
    }

    private String getReason(RestClientResponseException e) {

        String reason = null;

        try {
            Json json = Json.read(e.getResponseBodyAsString());

            if (json.has(ERROR_MESSAGE)) {
                reason = json.at(ERROR_MESSAGE).asString();
            } else {
                reason = json.toString();
            }
        } catch (Exception e1) {
            log.error("Cannot get the reason", e1);
            reason = e.getResponseBodyAsString();
        }

        return reason;
    }

}
