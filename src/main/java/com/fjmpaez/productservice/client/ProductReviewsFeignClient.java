package com.fjmpaez.productservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.fjmpaez.productservice.config.ProductReviewsFeignConfig;
import com.fjmpaez.productservice.pojo.Review;

@FeignClient(
        value = "product-reviews",
        url = "${product-reviews.ribbon.listOfServers:#{null}}",
        configuration = ProductReviewsFeignConfig.class)
public interface ProductReviewsFeignClient {

    @GetMapping("/v1.0/review/{product_id}")
    Review getReview(@RequestHeader("Client-Type") String clientType,
            @PathVariable("product_id") String productId);

}
