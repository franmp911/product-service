package com.fjmpaez.productservice.controller;

import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fjmpaez.productservice.service.ProductsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/v1.0/product")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @GetMapping(path = "/{product_id}")
    public Object getProduct(@NotBlank @PathVariable("product_id") String productId) {

        log.info("received request to getProduct - product_id: {}", productId);

        return productsService.getProduct(productId);
    }

}
