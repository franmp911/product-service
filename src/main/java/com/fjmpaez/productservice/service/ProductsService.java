package com.fjmpaez.productservice.service;

public interface ProductsService {

    Object getProduct(String productId);

}
