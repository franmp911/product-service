package com.fjmpaez.productservice.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fjmpaez.exceptionslib.DefinedErrorException;
import com.fjmpaez.exceptionslib.util.Utils;
import com.fjmpaez.productservice.client.AdidasClient;
import com.fjmpaez.productservice.client.ProductReviewsFeignClient;
import com.fjmpaez.productservice.pojo.Review;

import lombok.extern.slf4j.Slf4j;
import mjson.Json;

@Slf4j
@Service
public class ProductsServiceImpl implements ProductsService {

    private static final String CORRELATION_ID = "correlation_id";

    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    private Utils utils;

    @Autowired
    private AdidasClient adidasClient;

    @Autowired
    private ProductReviewsFeignClient productReviewsFeignClient;

    public Object getProduct(String productId) {

        Objects.requireNonNull(productId, "productId must not be null");

        Object result = null;

        log.trace("getProduct {}", productId);

        Json json = adidasClient.getProduct(productId);

        log.debug("response from external client: {}", json);

        if (Objects.nonNull(json) && !json.isNull()) {

            try {

                Review review = productReviewsFeignClient.getReview(applicationName, productId);

                log.debug("response from feign client: {}", review);

                if (Objects.nonNull(review)) {
                    json.set(Review.REVIEWS_AVG_SCORE, review.getAvgScore());
                    json.set(Review.REVIEWS_COUNT, review.getCount());
                }

            } catch (DefinedErrorException e) {

                if (HttpStatus.NOT_FOUND.equals(e.getHttpStatus())) {
                    log.info("There is no review for the productId:{}", productId, e);
                } else {
                    log.warn("Error getting review for the productId:{}", productId, e);
                }

            } catch (Exception e) {
                log.warn("Error getting review for the productId:{}", productId, e);
            }

            json.set(CORRELATION_ID, utils.getCurrentCorrelationId());

            result = json.getValue();

            log.trace("getProduct - response {}", result);
        }

        return result;
    }

}
