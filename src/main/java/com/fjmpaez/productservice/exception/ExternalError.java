package com.fjmpaez.productservice.exception;

import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fjmpaez.exceptionslib.DefinedErrorException;

import feign.Response;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExternalError {

    private static final ObjectMapper mapper = new ObjectMapper();

    private String description;

    @JsonProperty("external_error_code")
    private String externalErrorCode;

    @JsonProperty("error_code")
    private String errorCode;

    @JsonProperty("correlation_id")
    private String correlationId;

    public ExternalError(Response response) {

        super();

        try {

            String bodyAsString = IOUtils.toString(response.body().asInputStream(),
                    StandardCharsets.UTF_8);

            ExternalError error = mapper.readValue(bodyAsString, ExternalError.class);

            this.setDescription(error.getDescription());
            this.setExternalErrorCode(error.getExternalErrorCode());
            this.setErrorCode(error.getErrorCode());
            this.setCorrelationId(error.getCorrelationId());

        } catch (Exception e) {
            throw new DefinedErrorException();
        }
    }

}
