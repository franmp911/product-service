package com.fjmpaez.productservice.exception;

import org.springframework.http.HttpStatus;

import com.fjmpaez.exceptionslib.ErrorPrinter;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorEnum implements ErrorPrinter {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "000", null, "internal server error"),

    EXTERNAL_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "001", "{0}", "external error: {1}"),

    INVALID_REQUEST(HttpStatus.BAD_REQUEST, "002", null, "invalid parameter: {0}"),

    NOT_FOUND(HttpStatus.NOT_FOUND, "003", null, "product not found"),

    TIMEOUT(HttpStatus.GATEWAY_TIMEOUT, "010", null, "requesting to {0} timed out");

    private final HttpStatus httpStatus;
    private final String errorCode;
    private final String externalErrorCode;
    private final String description;
}
