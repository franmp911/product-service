package com.fjmpaez.productservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fjmpaez.exceptionslib.DefinedErrorException;

import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProductReviewsErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder defaultErrorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {

        if (response.status() >= HttpStatus.BAD_REQUEST.value()
                && response.status() <= HttpStatus.INTERNAL_SERVER_ERROR.value()) {

            log.error("Error calling {}", methodKey);

            ExternalError externalError = new ExternalError(response);

            DefinedErrorException ex = new DefinedErrorException();

            ex.setHttpStatus(HttpStatus.valueOf(response.status()));
            ex.setDescription(externalError.getDescription());
            ex.setErrorCode(externalError.getErrorCode());
            ex.setExternalErrorCode(externalError.getExternalErrorCode());

            return ex;
        }

        return defaultErrorDecoder.decode(methodKey, response);
    }

}